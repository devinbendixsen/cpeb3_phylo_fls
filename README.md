# CPEB3_Phylo_FLS

Bendixsen et al. (in review) Python data (.p files) and scripts used for data analysis of high-throughput sequencing data. Data depicts an experimentally resurrected and reconstructed phylogenetic fitness landscape for the naturally occurring CPEB3 self-cleaving ribozyme. Raw sequencing data is deposited at the European Nucleotide Archive (ENA) Project no: **PRJEB36228**.


## Installation

The directory is formatted as spyder python project.


## Usage

Python scripts in this directory encompass two categories:\
**(1)** Script that calculated the relative activity (fitness) of 27,648 genotypes for self-cleavage from high-throughput sequencing assays.\
**(2)** Scripts that analyze the relative activity data and create figures.
    
Data in this directory encompass two categories:\
**(1)** Excel spreadsheets (.xlsx) that contain either high-throughput sequencing statistics.\
**(2)** Python pickle files (.p) that contain calculated metrics.
    
Figures presented in Bendixsen et al. (in review) are found in the Figures directory.

## Authors and acknowledgment

Python scripts were developed by **Devin Bendixsen, PhD** in collaboration with **Tanner Pollock** and **Eric Hayden, PhD**.