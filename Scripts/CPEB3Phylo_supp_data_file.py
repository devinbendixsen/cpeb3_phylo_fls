#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 09:56:16 2019

@author: devin
"""

import numpy as np
import pickle
import matplotlib.pyplot as plt
import csv
import seaborn as sns
fitness=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_cleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_cleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_cleaved3.p","rb"))
uncleaved1=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved1.p","rb"))
uncleaved2=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved2.p","rb"))
uncleaved3=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved3.p","rb"))


#%%

DATA1=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved1.p','rb'))
DATA2=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved2.p','rb'))
DATA3=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved3.p','rb'))
DATA_fit=pickle.load(open('../Pickle/CPEB3Phylo_fitness.p','rb'))
delta={}
for seq in DATA_fit:
    delta[seq]=(np.std((DATA1[seq],DATA2[seq],DATA3[seq])))

#%%

with open('../DATA/CPEB3Phylo_fitness_table.csv','w', newline='') as csvfile:
    for seq in fitness:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([seq,fitness[seq],delta[seq],DATA1[seq],cleaved1.get(seq,0),uncleaved1[seq],DATA2[seq],cleaved2.get(seq,0),uncleaved2[seq],DATA3[seq],cleaved3.get(seq,0),uncleaved3[seq]])
