#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 15 19:31:01 2018

@author: devin
"""

import numpy as np
import pickle
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
#%%
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
    return mut

#%%
WT='GCCGGCGTTGGTG'
mutdist={}
for seq in CPEB3Phylo:
    mutdist[seq]=mutcalc(seq,WT)
#%%
x=[]
y=[]
size=[]
for seq in mutdist:
    x.append(mutdist[seq])
    y.append(CPEB3Phylo[seq])
    size.append((150*CPEB3Phylo[seq]))
#%%
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
#%%
xmean=[]
for i in range(0,14):
    z=[]
    for seq in CPEB3Phylo:
        if mutdist[seq]==i:
            z.append(CPEB3Phylo[seq])
    xmean.append(np.mean(z))
#%%
import seaborn as sns
plt.figure(figsize=[8,4])
plt.plot([0,1,2,3,4,5,6,7,8,9,10,11,12,13],xmean,color='black',linestyle='dashed',linewidth=3,zorder=1)
plt.scatter(x,y,c=y, cmap='RdBu',edgecolors='black',s=size,linewidths=1.2,zorder=2)
plt.ylabel('ribozyme activity',weight='bold',fontsize=15,color='black')
plt.xlabel('mutations from ancestral',weight='bold',fontsize=15,color='black')
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13])
sns.despine(trim=True)
plt.savefig('../Figures/CPEB3Phylo_mutdrop.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% MARMOSET PATH
sequences=[]
from itertools import product
dict={1:['G'], 2: ['C','G'], 3:['C'], 4:['G','A'], 5:['G','T'], 6:['C','T'], 7:['G'], 8:['T'], 9:['T'], 10:['G','A'], 11:['G'], 12:['T'], 13:['G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
subgraph2=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    subgraph2.append(seq)

#%%

WT='GCCGGCGTTGGTG'
mutdist={}
for seq in subgraph2:
    mutdist[seq]=mutcalc(seq,WT)
x=[]
y=[]
size=[]
for seq in mutdist:
    x.append(mutdist[seq])
    y.append(CPEB3Phylo[seq])
    size.append((150*CPEB3Phylo[seq]))
xmean=[]
for i in range(0,6):
    z=[]
    for seq in subgraph2:
        if mutdist[seq]==i:
            z.append(CPEB3Phylo[seq])
    xmean.append(np.mean(z))
import seaborn as sns
plt.figure(2,figsize=[8,4])
#plt.plot([0,1,2,3,4,5],xmean,color='black',linestyle='dashed',linewidth=3,zorder=1)
plt.scatter(x,y,c=y, cmap='RdBu',edgecolors='black',s=80,linewidths=1.2,zorder=2)
plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0])
plt.ylabel('ribozyme activity',weight='bold',fontsize=15,color='black')
plt.xlabel('mutations from ancestral',weight='bold',fontsize=15,color='black')
plt.xticks([0,1,2,3,4,5])
sns.despine(trim=True)
plt.savefig('../Figures/CPEB3Phylo_mutdrop_marmoset.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
import matplotlib.cm as cm
x=[0,1,2,3,4]
color={0:0.535,1:0.037,2:0.033,3:0.016,4:0.776}
for num in x:
    plt.scatter(num,1,color=cm.RdBu([color[num]]),s=200)
plt.savefig('../Figures/CPEB3Phylo_gel_color.png',bbox_inches='tight',dpi=1000,transparent=True)
    
    
    
    
    
    