#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 28 21:43:08 2018

@author: devin
"""

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)

fig = plt.figure(figsize=[8,6])
ax = fig.gca(projection='3d')
X, Y, Z = axes3d.get_test_data(0.05)
ax.plot_surface(X, Y, Z, rstride=1, cstride=1, alpha=1,cmap=cm.RdBu)
#cset = ax.contourf(X, Y, Z, zdir='z', offset=-100, cmap=cm.RdBu)
#cset = ax.contourf(X, Y, Z, zdir='x', offset=-40, cmap=cm.RdBu)
#cset = ax.contourf(X, Y, Z, zdir='y', offset=40, cmap=cm.RdBu)
#ax.set_xlabel('genotype space', weight='bold',fontsize=15)
#ax.set_xlim(-34, 34)
plt.xticks([],[])#plt.xticks([-30,-20,-10,0,10,20,30],[])
#ax.set_ylabel('genotype space', weight='bold',fontsize=15)
#ax.set_ylim(-34, 34)
plt.yticks([],[])#plt.yticks([-30,-20,-10,0,10,20,30],[])
#ax.set_zlabel('fitness (phenotype)', weight='bold',fontsize=15)
#ax.set_zlim(-100, 100)
ax.set_zticks([])#ax.set_zticks([-100,-75,-50,-25,0,25,50,75,100])
ax.set_axis_off()
plt.savefig('../Figures/CPEB3Phylo_example_landscape.png',bbox_inches='tight',dpi=1000,transparent=True)