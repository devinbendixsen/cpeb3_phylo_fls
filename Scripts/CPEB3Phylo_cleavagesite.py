#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 09:45:23 2018

@author: devin
"""

# =============================================================================
# Master code to analyze CPEB3Phylo high throughput sequencing data.
# GOALS - To determine if there is a cleavage site shift in the CPEB3Phylo library.
# =============================================================================
#Import needed packages
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
#%%
#Define initial variables
library='CPEB3Phylo'
rz_WT_cleaved = 'GGGGGCCACAGCAGAAGCGTTCACGTCGCGGCCCCTGTCAGATTCTTGTGAATCTGCGAATTCTGCT'
rz_cleaved_seq = 'AAGGACCATTC'
weeks = 'GTTCGATCCGGTTCGC'
mutations=[1,6,9,19,25,29,30,36,46,47,48,49,58] #location of expected mutations
CPEB3Phylo_sequences=pickle.load(open('../Pickle/CPEB3Phylo_sequences.p','rb'))
TSO='CATGCATGCGGG'
#%%
##Identify where the data is stored.
seqDATA={}
seqDATA[1]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo1.fastq"
seqDATA[2]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo2.fastq"
seqDATA[3]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo3.fastq"

#%%
num=0
DATA={}
for i in range(1,4):#builds a nested dictionary DATA with rz_cleaved, rz_uncleaved and relative_cleavage for each replicate
    DATA["rz_cleaved_len{}".format(i)] = []
    n=0
    for sequence in open (seqDATA[i]): #opens fastq sequencing files and extracts the 2nd line (sequence)
        n+=1
        if n==2:
            n=-2
            if weeks in sequence and TSO in sequence and len(sequence)>=(len(rz_WT_cleaved)+len(rz_cleaved_seq)+1) and 'CTATAGGA' not in sequence: #verifies that the weeks linker is in the sequence and determines that the length is long enough and the TATA box is not in it
                sequence=sequence.split(weeks,1)[0] #extracts the sequence upstream of the weeks linker
                if len(sequence)>30:
                    sequence=sequence.split(TSO,1)[1] #extracts the sequence upstream of the weeks linker
                    DATA["rz_cleaved_len{}".format(i)].append(len(sequence))

#%%
                    
plt.figure(figsize=[12,3])
for i in range(1,4):
    plt.subplot(1,3,i)
    sns.kdeplot(DATA["rz_cleaved_len{}".format(i)],shade=True)
plt.tight_layout()
    
#%%
for i in range(1,4):
    DATA["cleaved{}".format(i)]=[]
    for num in DATA["rz_cleaved_len{}".format(i)]:
        if num!=82 and num!=81 and num!=83:
            DATA["cleaved{}".format(i)].append(num)
#%%
plt.figure(figsize=[8,4])
for i in range(1,4):
    plt.subplot(3,1,i)
    sns.kdeplot(DATA["cleaved{}".format(i)],shade=True)
    plt.xlim(0,120)
    #plt.axvline(np.mean(DATA["cleaved{}".format(i)]),linestyle='dashed',color='black',zorder=0)
plt.tight_layout()
#%%
plt.figure(figsize=[8,3])
for i in range(1,4):
    sns.kdeplot(DATA["cleaved{}".format(i)],shade=True, alpha=0.3)
plt.tight_layout()

#%%
x=0
for i in range(1,4):
    for num in DATA["cleaved{}".format(i)]:
        if num==38:
            x+=1
    print(x)