#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 14:48:43 2018

@author: devin
"""

sequences=[]
from itertools import product
dict={1:['A','C','G'], 2: ['C','G'], 3:['C','T'], 4:['A','G'], 5:['T','G'], 6:['C','T'], 7:['A','G'], 8:['C','T'], 9:['A','T'], 10:['A','T','G'], 11:['A','G'], 12:['C','G','T'], 13:['A','G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
sequencesCPEB3Phylo=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    sequencesCPEB3Phylo.append(seq)
    
import pickle
pickle.dump(sequencesCPEB3Phylo, open("../Pickle/CPEB3Phylo_sequences.p", "wb"))