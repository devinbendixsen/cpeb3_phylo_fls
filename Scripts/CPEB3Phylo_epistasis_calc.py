#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 12:45:54 2018

@author: devin
"""

import numpy as np
import pickle
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
#%%        
def mutcalc(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
        if mut>2:
            break
    return mut
"""
data=CPEB3Phylo
epistasis=[]
start=[]
#data={'AA':1,'AB':1.5,'BA':1.5, 'BB':2}
for seq in data:
    for seq2 in data:
        if mutcalc(seq,seq2)==2:
            mutAB=[]
            for seq3 in data:
                if mutcalc(seq,seq3)==1 and mutcalc(seq2,seq3)==1:
                    mutAB.append(seq3)
            epsilon=np.log10((data[seq]*data[seq2])/(data[mutAB[0]]*data[mutAB[1]]))
            epistasis.append(epsilon)
pickle.dump(epistasis, open("../Pickle/CPEB3Phylo_epistasis.p", "wb"))
"""
#%%
plt.rcParams['patch.edgecolor'] = 'black'
plt.figure(figsize=[2,3])
epistasis=[]
epistasis_all=pickle.load(open("../Pickle/CPEB3Phylo_epistasis.p","rb"))
for num in epistasis_all:
    if num>=0:
        epistasis.append(num)
import seaborn as sns
sns.violinplot(epistasis, orient='v',edgecolor='black',color='teal',linewidth=2,linecolor='black')
sns.despine(trim=True)
plt.xticks([0],['epistasis'],weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.ylabel('severity',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/CPEB3Phylo_epistasis.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure (2,figsize=[6.5,3])
y=[0.4492,0.0961,0.0284]
x=[0,1,2]
plt.bar(x,y, edgecolor='black',color='teal',linewidth=2)
plt.yticks([0.0,0.1,0.2,0.3,0.4,0.5],weight='bold',fontsize=15,color='black')
plt.ylabel('proportion of squares',weight='bold',fontsize=15,color='black')
sns.despine(trim=True)
plt.xticks([0,1,2],['magnitude','simple sign','reciprocal sign'],weight='bold',fontsize=15,color='black')

plt.savefig('../Figures/CPEB3Phylo_epistasis_geno.png',bbox_inches='tight',dpi=1000,transparent=True)