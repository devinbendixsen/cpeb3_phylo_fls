#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 14:41:27 2020

@author: devin
"""
# =============================================================================
# Script to plot a local fitness landscape depicting all mutational trajectories
# from a central (WT) genotype to 4 mutations away
# =============================================================================
import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
cmap = plt.cm.get_cmap('RdBu')
fitness=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))

#%% function to calculate the mutational distance between two genotypes
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
            
    return mut
#%% set variables WT and fitness cutoff
genotype_name={'GCCGGCATTGGTG':'human','CCCGGCGTTGGTA':'panda','ACCGGCGTTGGTG':'rat', 'GCCGGCGTTGGTG':'ancestral','GCCGGCGTTGGCG':'mouse'}

WT='GCCGGCATTGGTG' #human
#WT='ACCGGCGTTGGTG' #rat
#WT='GCCGGCGTTGGCG' #mouse
#WT='CCCGGCGTTGGTA' #giant panda
#WT='GCCGGCGTTGGTG' #ancestral
cutoff=0.025

#%% Determines the mutational distance for all genotypes within the library that is within 4 mutations of WT genotype
mutdist={}
for seq in fitness:
    if mutcalc(seq,WT)<=4:
        mutdist[seq]=mutcalc(seq,WT)

#%% Calculates the theta directions for all mutational distances
num1=0
num2=0
num3=0
num4=0
for seq in mutdist:
    if mutdist[seq]==1:
        num1+=1
        for seq2 in mutdist:
            if mutdist[seq2]==2 and mutcalc(seq,seq2)==1:
                num2+=1
                for seq3 in mutdist:
                    if mutdist[seq3]==3 and mutcalc(seq2,seq3)==1:
                        num3+=1
                        for seq4 in mutdist:
                            if mutdist[seq4]==4 and mutcalc(seq3,seq4)==1:
                                num4+=1
theta1 = list(np.linspace(0, 2*np.pi, num1))
theta2 = list(np.linspace(0, 2*np.pi, num2))
theta3 = list(np.linspace(0, 2*np.pi, num3))
theta4 = list(np.linspace(0, 2*np.pi, num4))

#%% Plots the mutational trajectories
plt.figure(figsize=[4,4])

if WT=='GCCGGCGTTGGTG':
    plt.scatter(0,0,color=cmap(fitness[WT]),s=50,zorder=5,linewidth=1,edgecolor='black',marker='s')
else:
    plt.scatter(0,0,color=cmap(fitness[WT]),s=50,zorder=5,linewidth=1,edgecolor='black',marker='o')
    
num1=0
num2=0
num3=0
num4=0
for seq in mutdist:
    if mutdist[seq]==1:
        if fitness[seq]>=cutoff:
            plt.plot([0,1*np.cos(theta1[num1])],[0,1*np.sin(theta1[num1])],color=cmap(fitness[seq]),zorder=1,linewidth=1)
        num1+=1
        for seq2 in mutdist:
            if mutdist[seq2]==2 and mutcalc(seq,seq2)==1:
                if fitness[seq2]>=cutoff and fitness[seq]>=cutoff:
                    plt.plot([1*np.cos(theta1[num1-1]),2*np.cos(theta2[num2])],[1*np.sin(theta1[num1-1]),2*np.sin(theta2[num2])],color=cmap(fitness[seq2]),zorder=2,linewidth=1)
                num2+=1
                for seq3 in mutdist:
                    if mutdist[seq3]==3 and mutcalc(seq2,seq3)==1:
                        if fitness[seq3]>=cutoff and fitness[seq]>=cutoff and fitness[seq2]>=cutoff:
                            plt.plot([2*np.cos(theta2[num2-1]),3*np.cos(theta3[num3])],[2*np.sin(theta2[num2-1]),3*np.sin(theta3[num3])],color=cmap(fitness[seq3]),zorder=3,linewidth=1)
                        num3+=1
                        for seq4 in mutdist:
                            if mutdist[seq4]==4 and mutcalc(seq3,seq4)==1:
                                if fitness[seq4]>=cutoff and fitness[seq]>=cutoff and fitness[seq2]>=cutoff and fitness[seq3]>=cutoff:
                                    plt.plot([3*np.cos(theta3[num3-1]),4*np.cos(theta4[num4])],[3*np.sin(theta3[num3-1]),4*np.sin(theta4[num4])],color=cmap(fitness[seq4]),zorder=4,linewidth=1)
                                num4+=1
plt.xlim(-4,4)
plt.ylim(-4,4)
plt.xticks([])
plt.yticks([])
sns.despine(left='off',bottom='off')
plt.savefig('../Figures/CPEB3Phylo_{}_localscape_{}.png'.format(genotype_name[WT],cutoff),bbox_inches='tight',dpi=1000,transparent=True)


