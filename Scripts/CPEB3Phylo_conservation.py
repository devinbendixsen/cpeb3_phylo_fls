#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 22 17:34:03 2018

@author: devin
"""
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.cm as cm
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 13}
plt.rc('font', **font)
#%%
conservation=[76	,94,98,97,95,96,97,97,85,96,97,98,98,95,98,97,96,93,83,97,98,99,97,96,87,99,91,96,86,91,100,96,99,97,97,98,99,100,99,99,99,100,100,97,100,88,88,82,89,99,100,99,99,99,99,99,92,92,99,97,99,97,96,97,98,99,99]
artiodactyla=[27	,100,91,91,100,100,82,100,36,100,100,91,100,100,100,100,100,100,27,100,100,100,100,100,73,100,64,100,73,82,100,91,100,100,91,100,100,100,100,100,91,100,100,100,100,100,100,100,100,100,100,100,100,91,100,100,100,91,100,100,100,100,100,100,100,100,100]
carnivora=[40,90,100,100,70,100,100,100,100,100,100,100,100,90,100,100,100,60,70,100,80,80,80,70,70,80,70,70,50,100,80,100,100,100,80,80,80,80,80,80,80,80,80,70,100,70,90,80,80,80,100,80,80,80,80,80,70,50,100,80,100,100,100,100,100,100,100]
primates=[97,100,100,100,100,93,100,100,100,100,100,100,100,100,100,100,100,100,97,100,100,100,100,100,97,100,100,100,93,93,100,100,100,100,100,97,100,100,100,100,100,100,100,100,100,100,97,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
rodentia=[92,96,100,96,100,100,100,96,96,96,96,100,100,96,100,100,100,96,100,100,100,100,100,96,96,100,100,96,96,96,100,96,100,100,100,100,100,100,100,96,100,100,100,100,100,96,100,56,68,100,100,100,100,100,100,100,96,96,100,100,100,100,100,100,100,100,100]
#%%
x=list(range(1,68))
color=[]
for seq in conservation:
    color.append(cm.YlGnBu((seq-70)/30))

plt.figure(1,figsize=[12,8])
plt.subplot(511)
plt.hlines([0,50,100],xmin=1,xmax=67,zorder=0,linestyle='dashed',color='grey')
plt.scatter(x,conservation,color=color,edgecolor='black',s=75)
plt.yticks([0,50,100],['0%','50%','100%'])
plt.xticks([1,5,10,15,20,25,30,35,40,45,50,55,60,65,67],[])

plt.ylim(-5,110)
plt.ylabel('conservation',weight='bold',fontsize=13)

color=[]
for seq in artiodactyla:
    color.append(cm.YlGnBu((seq-70)/30))

plt.subplot(514)

plt.hlines([0,50,100],xmin=1,xmax=67,zorder=0,linestyle='dashed',color='grey')
plt.scatter(x,artiodactyla,color=color,edgecolor='black',s=75)
plt.yticks([0,50,100],['0%','50%','100%'])
plt.xticks([1,5,10,15,20,25,30,35,40,45,50,55,60,65,67],[])
plt.ylim(-5,110)

color=[]
for seq in carnivora:
    color.append(cm.YlGnBu((seq-70)/30))

plt.subplot(515)

plt.hlines([0,50,100],xmin=1,xmax=67,zorder=0,linestyle='dashed',color='grey')
plt.scatter(x,carnivora,color=color,edgecolor='black',s=75)
plt.yticks([0,50,100],['0%','50%','100%'])
plt.xticks([1,5,10,15,20,25,30,35,40,45,50,55,60,65,67],[])
plt.ylim(-5,110)

color=[]
for seq in primates:
    color.append(cm.YlGnBu((seq-70)/30))

plt.subplot(512)

plt.hlines([0,50,100],xmin=1,xmax=67,zorder=0,linestyle='dashed',color='grey')
plt.scatter(x,primates,color=color,edgecolor='black',s=75)
plt.yticks([0,50,100],['0%','50%','100%'])
plt.xticks([1,5,10,15,20,25,30,35,40,45,50,55,60,65,67],[])
plt.ylim(-5,110)


color=[]
for seq in rodentia:
    color.append(cm.YlGnBu((seq-70)/30))

plt.subplot(513)

plt.hlines([0,50,100],xmin=1,xmax=67,zorder=0,linestyle='dashed',color='grey')
plt.scatter(x,rodentia,color=color,edgecolor='black',s=75)
plt.yticks([0,50,100],['0%','50%','100%'])
plt.xticks([1,5,10,15,20,25,30,35,40,45,50,55,60,65,67],[])
plt.ylim(-5,110)


sns.despine(trim=True,offset={'left':-25,'bottom':5})
plt.tight_layout(pad=1,w_pad=0.5, h_pad=1)
plt.savefig('../Figures/CPEB3Phylo_conservation.png',bbox_inches='tight',dpi=1000,transparent=True)