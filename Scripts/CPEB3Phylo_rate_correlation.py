#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 14:52:48 2018

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
from random import random, randint
import numpy as np
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
#%%
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': 'GCCGGCGTTGGTG',
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': 'GCCGGCGTTGGTG',
         'horse': 'GCCGGCGTTGGTG',
         'shrew': 'GCCGGCGTTGGTG',
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': 'GCCGGCGTTGGTG',
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': 'GCCGGCGTTGGTG',
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': 'GCCGGCGTTGGTG',
         'Philippine tarsier': 'GCCGGCGTTGGTG',
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': 'GCCGGCGTTGGTG',
         'white-cheeked gibbon': 'GCCGGCGTTGGTG',
         'sumatran orangutan':'GCCGGCGTTGGTG',
         'chimpanzee': 'GCCGGCGTTGGTG',
         'gorilla': 'GCCGGCGTTGGTG',
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}

for seq in species:
    print(seq,species[seq],CPEB3Phylo[species[seq]])
#%%
import pandas as pd
import seaborn as sns
from scipy import stats
previous={'gray short-tailed opossum':2.1,
          'African bush elephant':2.05,
          'European rabbit': 2.15,
          'house mouse':2.9,
          'rat':0.164,
          'human-SNP':0.69,
          'human':1.79,
          'chimpanzee':2.2}
x=[]
y=[]
xnum=[]
ynum=[]
for seq in previous:
    x.append(previous[seq])
    y.append(CPEB3Phylo[species[seq]])
    xnum.append(previous[seq])
    ynum.append(CPEB3Phylo[species[seq]])

datas=pd.DataFrame({'Replicate 1':x,'Replicate 2':y})
g=sns.JointGrid(x='Replicate 1',y='Replicate 2',data=datas)
g = g.plot_joint(plt.scatter, color='b',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="b")
g = g.plot_joint(sns.regplot, color='b')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
print ('p-value : ', p_value)
#plt.scatter(x,y,c=y,cmap='RdBu')
plt.ylabel('ribozyme activity',weight='bold',fontsize=15,color='black')
plt.xlabel('previous rate (kobs)',weight='bold',fontsize=15,color='black')
plt.ylim(0,)
plt.xlim(0,)

#%%
import pandas as pd
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
ancestor='GCCGGCGTTGGTG'
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': ancestor,
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': ancestor,
         'horse': ancestor,
         'shrew': ancestor,
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': ancestor,
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': ancestor,
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': ancestor,
         'Philippine tarsier': ancestor,
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': ancestor,
         'white-cheeked gibbon': ancestor,
         'sumatran orangutan': ancestor,
         'chimpanzee': ancestor,
         'gorilla': ancestor,
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
speciesdata=[]
#for seq in species:
#    print(seq,' : ',species[seq],' : ',CPEB3Phylo[species[seq]])
#    speciesdata.append(CPEB3Phylo[species[seq]])
speciesdata.append(CPEB3Phylo['ACCGGCGTTGGTG'])
df=pd.DataFrame(speciesdata)
plt.figure(figsize=[1,12])
sns.heatmap(data=df,cmap='RdBu',vmin=0,vmax=1)
plt.savefig('../Figures/CPEB3Phylo_speciesheatmap_legend.png',bbox_inches='tight',dpi=1000,transparent=True)