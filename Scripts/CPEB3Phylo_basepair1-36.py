#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 12:21:44 2018

@author: devin
"""

import pickle
import matplotlib.pyplot as plt
from random import random, randint
import numpy as np
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
ancestral='GCCGGCGTTGGTG'
#%%
sequences=[]
from itertools import product
dict={1:['G','A','C'], 2: ['C'], 3:['C'], 4:['G'], 5:['G'], 6:['C'], 7:['G','A'], 8:['T','C'], 9:['T'], 10:['G'], 11:['G'], 12:['T'], 13:['G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
subgraph1=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    subgraph1.append(seq)

nodes={}
for seq in subgraph1:
    nodes[seq]=CPEB3Phylo[seq]
    
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
    return mut
import seaborn as sns
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
x=[]
y=[]
for seq in nodes:
    x.append(mutcalc(seq,ancestral))
    y.append(CPEB3Phylo[seq])
plt.scatter(x,y, c=y, cmap='RdBu',s=100,edgecolor='black',linewidth=2)
plt.xticks([0,1,2,3])
plt.yticks([0.0,0.2,0.4,0.6,0.8,1])

sns.despine(trim=True)
plt.xlabel('mutations from ancestral',weight='bold',fontsize=15)
plt.ylabel('ribozyme activity',weight='bold',fontsize=15)

#%%
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': 'GCCGGCGTTGGTG',
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': 'GCCGGCGTTGGTG',
         'horse': 'GCCGGCGTTGGTG',
         'shrew': 'GCCGGCGTTGGTG',
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': 'GCCGGCGTTGGTG',
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': 'GCCGGCGTTGGTG',
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': 'GCCGGCGTTGGTG',
         'Philippine tarsier': 'GCCGGCGTTGGTG',
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': 'GCCGGCGTTGGTG',
         'white-cheeked gibbon': 'GCCGGCGTTGGTG',
         'sumatran orangutan':'GCCGGCGTTGGTG',
         'chimpanzee': 'GCCGGCGTTGGTG',
         'gorilla': 'GCCGGCGTTGGTG',
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}
for seq in species:
    print(seq,CPEB3Phylo[species[seq]])
    
#%%
n=0
for seq in CPEB3Phylo:
    if CPEB3Phylo[seq]<=0.03:
        n+=1
print(n)