#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 14:18:04 2018

@author: devin
"""

import pickle
import matplotlib.pyplot as plt
from random import random, randint
import numpy as np
import os
import sys
import seaborn as sns
#%%
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
#%%
import pandas as pd
#for seq in CPEB3Phylo:
#    x=np.log10(CPEB3Phylo[seq])
#    CPEB3Phylo[seq]=x

#%%
plt.figure(figsize=[8,4])
y=pd.Series(list(CPEB3Phylo.values()))
ax=sns.distplot(y, color='royalblue',hist=True,rug=True,kde=True)
plt.ylabel('kernal density estimate',weight='bold',fontsize=15,color='black')
#plt.xticks([-2,-1,0],[0.01,0.1,1])
plt.xlabel('ribozyme activity',weight='bold',fontsize=15,color='black')
plt.axvline(CPEB3Phylo['GCCGGCGTTGGTG'],ls='dashed',color='black')
plt.savefig('../Figures/CPEB3Phylo_fit_distributions.png',bbox_inches='tight',dpi=1000,transparent=True)