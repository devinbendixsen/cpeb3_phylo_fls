#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 17:30:50 2018

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
from random import random, randint
import numpy as np
import seaborn as sns
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))

#%%
mutfit={}
nucleotides=['A','C','T','G']
for i in range(0,13):
    for nuc in nucleotides:
        mutfit[str(i)+nuc]=[]
        #%%
for seq in CPEB3Phylo:
    for i in range(0,13):
        mutfit[str(i)+seq[i]].append(CPEB3Phylo[seq])
#%%
from scipy import stats
meanfit={}
stdev={}
for mut in mutfit:
    if len(mutfit[mut])>0:
        meanfit[mut]=np.mean(mutfit[mut])
        stdev[mut]=stats.sem(mutfit[mut])
#%%
shape={}
WT='GCCGGCGTTGGTG'
for mut in meanfit:
    d=mut[-1]
    e=mut.rsplit(d)
    x=int(e[0])
    if d==WT[x]:
        shape[mut]='s'
    else:
        shape[mut]='o'
#%%
plt.figure(figsize=[8,4])
color={'A':'blue','C':'yellow','T':'red','G':'green'}
marker={'A':'o','C':'d','T':'s','G':'v'}
for mut in meanfit:
    d=mut[-1]
    e=mut.rsplit(d)
    x=e[0]
    plt.scatter(x,meanfit[mut],color=color[d],edgecolor='black',linewidth=2,s=120,marker=shape[mut])
plt.yticks([0,0.05,0.10,0.15])
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12],[1,6,9,19,25,29,30,36,46,47,48,49,58])
plt.ylabel('mean ribozyme activity',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/CPEB3Phylo_mutanalysis.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
colororder=[]
ticks=[]
for seq in meanfit:
    colororder.append(color[seq[-1]])
    ticks.append(seq[-1])
for i in range(len(ticks)):
    if ticks[i]=='T':
        ticks[i]='U'
#%%

plt.figure(figsize=[12,4])

x=[]
y=[]
for mut in mutfit:
    if len(mutfit[mut])>0:
        for i in range(0,len(mutfit[mut])):
            x.append(mut)
            y.append(mutfit[mut][i])
sns.violinplot(x,y,plotorder=[0,1,2,3,4,5,6,7,8,9,10,11,12],scale='width',palette=colororder)
j=list(range(0,29))
plt.xticks(j,ticks)
plt.ylabel('ribozyme activity',weight='bold',fontsize=15,color='black')
sns.despine(trim=True, offset=5)
plt.savefig('../Figures/CPEB3Phylo_mutanalysis_dist.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
import pandas as pd
df=pd.DataFrame(list(meanfit.values()))
plt.figure(figsize=[1,12])
sns.heatmap(data=df,cmap='RdBu')
plt.savefig('../Figures/CPEB3Phylo_mutanalysis_heatmap.png',bbox_inches='tight',dpi=1000)
#%%
