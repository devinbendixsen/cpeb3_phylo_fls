#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 11:33:46 2018

@author: devin
"""

import numpy as np
import pickle
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
#%%
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
    return mut
#%%
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': 'GCCGGCGTTGGTG',
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': 'GCCGGCGTTGGTG',
         'horse': 'GCCGGCGTTGGTG',
         'shrew': 'GCCGGCGTTGGTG',
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': 'GCCGGCGTTGGTG',
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': 'GCCGGCGTTGGTG',
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': 'GCCGGCGTTGGTG',
         'Philippine tarsier': 'GCCGGCGTTGGTG',
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': 'GCCGGCGTTGGTG',
         'white-cheeked gibbon': 'GCCGGCGTTGGTG',
         'sumatran orangutan':'GCCGGCGTTGGTG',
         'chimpanzee': 'GCCGGCGTTGGTG',
         'gorilla': 'GCCGGCGTTGGTG',
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}
extant=list(species.values())
#%% MARSUPIALS
sequences=[]
from itertools import product
dict={1:['G'], 2: ['C'], 3:['C','T'], 4:['G'], 5:['G'], 6:['C'], 7:['G'], 8:['T'], 9:['A','T'], 10:['T','G'], 11:['A','G'], 12:['C','G','T'], 13:['G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
subgraph1=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    subgraph1.append(seq)

nodes={}
for seq in subgraph1:
    nodes[seq]=CPEB3Phylo[seq]

edges={}
n=1
for seq in nodes:
    for seq2 in nodes:
        if mutcalc(seq,seq2)==1:
            edges[n]=[seq,seq2]
            n+=1

import csv 
with open('../Data/CPEB3Phylo_marsupial_edges.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Source','Target','Type'])
    for key in edges:
        spamwriter.writerow([edges[key][0], edges[key][1],'undirected'])

with open('../Data/CPEB3Phylo_marsupial_nodes.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Id','fitness','[z]','extant'])
    for seq in nodes:
        if seq in extant:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'extant'])
        else:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'not-extant'])
#%% PRIMATES
sequences=[]
from itertools import product
dict={1:['G'], 2: ['C','G'], 3:['C'], 4:['G','A'], 5:['G','T'], 6:['C','T'], 7:['G','A'], 8:['T','C'], 9:['T'], 10:['G','A'], 11:['G'], 12:['T'], 13:['G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
subgraph2=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    subgraph2.append(seq)

nodes={}
for seq in subgraph2:
    nodes[seq]=CPEB3Phylo[seq]

edges={}
n=1
for seq in nodes:
    for seq2 in nodes:
        if mutcalc(seq,seq2)==1:
            edges[n]=[seq,seq2]
            n+=1
import csv 
with open('../Data/CPEB3Phylo_primates_edges.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Source','Target','Type'])
    for key in edges:
        spamwriter.writerow([edges[key][0], edges[key][1],'undirected'])

with open('../Data/CPEB3Phylo_primates_nodes.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Id','fitness','[z]','extant'])
    for seq in nodes:
        if seq in extant:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'extant'])
        else:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'not-extant'])

#%% WITHOUT NEUTRAL
sequences=[]
from itertools import product
dict={1:['A','C','G'], 2: ['C','G'], 3:['C','T'], 4:['A','G'], 5:['T','G'], 6:['C','T'], 7:['A','G'], 8:['C','T'], 9:['T'], 10:['G'], 11:['G'], 12:['T'], 13:['A','G']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10], dict[11], dict[12], dict[13]):
    sequences.append(x)
#print sequences
notneutral=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])+str(x[10])+str(x[11])+str(x[12])
    notneutral.append(seq)
nodes={}
for seq in notneutral:
    nodes[seq]=CPEB3Phylo[seq]

edges={}
n=1
for seq in nodes:
    for seq2 in nodes:
        if mutcalc(seq,seq2)==1:
            edges[n]=[seq,seq2]
            n+=1
import csv 
with open('../Data/CPEB3Phylo_notneutral_edges.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Source','Target','Type'])
    for key in edges:
        spamwriter.writerow([edges[key][0], edges[key][1],'undirected'])

with open('../Data/CPEB3Phylo_notneutral_nodes.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Id','fitness','[z]','extant'])
    for seq in nodes:
        if seq in extant:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'extant'])
        else:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'not-extant'])
#%%
n=0
for seq in species:
    if CPEB3Phylo[species[seq]]>0.0 and CPEB3Phylo[species[seq]]<=0.2:
        print(seq)