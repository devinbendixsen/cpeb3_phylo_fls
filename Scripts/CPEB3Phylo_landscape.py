#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 12:28:57 2018

@author: devin
"""

import numpy as np
import pickle
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
#%%
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
    return mut
#%%
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': 'GCCGGCGTTGGTG',
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': 'GCCGGCGTTGGTG',
         'horse': 'GCCGGCGTTGGTG',
         'shrew': 'GCCGGCGTTGGTG',
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': 'GCCGGCGTTGGTG',
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': 'GCCGGCGTTGGTG',
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': 'GCCGGCGTTGGTG',
         'Philippine tarsier': 'GCCGGCGTTGGTG',
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': 'GCCGGCGTTGGTG',
         'white-cheeked gibbon': 'GCCGGCGTTGGTG',
         'sumatran orangutan':'GCCGGCGTTGGTG',
         'chimpanzee': 'GCCGGCGTTGGTG',
         'gorilla': 'GCCGGCGTTGGTG',
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}
extant=list(species.values())
#%%

nodes={}
for seq in CPEB3Phylo:
    nodes[seq]=CPEB3Phylo[seq]


edges={}
n=1
for seq in nodes:
    for seq2 in nodes:
        if mutcalc(seq,seq2)==1:
            edges[n]=[seq,seq2]
            n+=1
#%%
import csv 
with open('../Data/CPEB3Phylo_edges.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Source','Target','Type'])
    for key in edges:
        spamwriter.writerow([edges[key][0], edges[key][1],'undirected'])

with open('../Data/CPEB3Phylo_nodes.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Id','fitness','[z]','extant'])
    for seq in nodes:
        if seq in extant:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'extant'])
        else:
            spamwriter.writerow([seq, CPEB3Phylo[seq],CPEB3Phylo[seq],'not-extant'])