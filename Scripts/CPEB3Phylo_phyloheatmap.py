#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 13:55:25 2018

@author: devin
"""
import pandas as pd
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
CPEB3Phylo=pickle.load(open("../Pickle/CPEB3Phylo_fitness.p","rb"))
ancestor='GCCGGCGTTGGTG'
species={'gray short-tailed opossum':'GCTGGCGTATAGG',
         'brushtail possum':'GCTGGCGTATACG',
         'Tammar wallaby':'GCTGGCGTATATG',
         'African bush elephant': ancestor,
         'rock hyrax': 'GCCGGCGTTTGTG',
         'alpaca': ancestor,
         'horse': ancestor,
         'shrew': ancestor,
         'giant panda': 'CCCGGCGTTGGTA',
         'northern treeshrew': ancestor,
         'American pike': 'GCCGGCGTTGATG',
         'European rabbit': ancestor,
         'guinea pig': 'GCCGGCGTTGATG',
         'house mouse': 'GCCGGCGTTGGCG',
         'rat': 'ACCGGCGTTGGTG',
         'grey mouse lemur': ancestor,
         'Philippine tarsier': ancestor,
         'marmoset': 'GGCATTGTTAGTG',
         'rhesus macaque': ancestor,
         'white-cheeked gibbon': ancestor,
         'sumatran orangutan': ancestor,
         'chimpanzee': ancestor,
         'gorilla': ancestor,
         'human':     'GCCGGCATTGGTG',
         'human-SNP': 'GCCGGCACTGGTG'}

speciesdata=[]
for seq in species:
    print(seq,' : ',species[seq],' : ',CPEB3Phylo[species[seq]])
    speciesdata.append(CPEB3Phylo[species[seq]])
df=pd.DataFrame(speciesdata)
plt.figure(figsize=[1,12])
sns.heatmap(data=df,cmap='RdBu')
plt.savefig('../Figures/CPEB3Phylo_speciesheatmap.png',bbox_inches='tight',dpi=1000)

#%%
cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved3.p","rb"))
print(cleaved1['GCCGGCATTGGTG'])
print(cleaved2['GCCGGCATTGGTG'])
print(cleaved3['GCCGGCATTGGTG'])

cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_cleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_cleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_cleaved3.p","rb"))
print(cleaved1['GCCGGCATTGGTG'])
print(cleaved2['GCCGGCATTGGTG'])
print(cleaved3['GCCGGCATTGGTG'])

cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved3.p","rb"))
print(cleaved1['GCCGGCATTGGTG'])
print(cleaved2['GCCGGCATTGGTG'])
print(cleaved3['GCCGGCATTGGTG'])
#%%
import numpy as np
t=[]
c=[]
for seq in CPEB3Phylo:
    if seq[7]=='T':
        t.append(CPEB3Phylo[seq])
    elif seq[7]=='C':
        c.append(CPEB3Phylo[seq])
print(np.mean(t))
print(np.mean(c))