#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 09:33:47 2018

@author: devin
"""
import pickle
import numpy as np
import csv
import seaborn as sns
import matplotlib.pyplot as plt

DATA1=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved1.p','rb'))
DATA2=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved2.p','rb'))
DATA3=pickle.load( open('../Pickle/CPEB3Phylo_fraction_cleaved3.p','rb'))
DATA_fit=pickle.load(open('../Pickle/CPEB3Phylo_fitness.p','rb'))
#%%
delta={}
for seq in DATA_fit:
    delta[seq]=(np.std((DATA1[seq],DATA2[seq],DATA3[seq])))

    
#%%
sns.set_style('white')
deltavalues=list(delta.values())
deltavalues.sort(reverse=True)
plt.figure(figsize=[4,3])

x=[]
y=[]
for seq in delta:
    x.append(delta[seq])
    y.append(DATA_fit[seq])
plt.xticks(weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.scatter(x,y,color='gainsboro',edgecolors='black',s=20)
plt.xlabel('delta ribozyme activity',weight='bold',fontsize=15,color='black')
plt.ylabel('ribozyme activity',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/CPEB3Phylo_delta.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%


with open('../Data/CPEB3Phylo_genonets.csv','w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Genotypeset', 'Genotype', 'Score','Delta'])
    for seq in DATA_fit:
        spamwriter.writerow(['CPEB3Phylo', seq, DATA_fit[seq],delta[seq]])
