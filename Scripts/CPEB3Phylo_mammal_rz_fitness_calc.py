#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 09:12:42 2019

@author: devin
"""

# =============================================================================
# Generates the fitness of the 100 extant mammalian ribozyme sequences and the
# 96 inferred ancestral nodes
# =============================================================================
import pandas as pd
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)
sns.set_style('ticks')
DATA_fit=pickle.load(open('../Pickle/CPEB3Phylo_fitness.p','rb'))
ancestral='GGGGGCCACAGCAGAAGCGTTCACGTCGCGGCCCCTGTCAGATTCTGGTGAATCTGCGAATTCTGCT'
df = pd.read_excel ('../Data/mammal_rz_sequences.xlsx')
df.dropna()
mammals_rz=df.set_index('species')['sequence'].to_dict()

#%%

mutations=[1,6,9,19,25,29,30,36,46,47,48,49,58]
def mutcalc(seq,WT): #determines the number of mutations
    mut=[]
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut.append(i)
    return mut

seq_mut={}
for seq in mammals_rz:
    seq_mut[seq]=mutcalc(mammals_rz[seq],ancestral)

mut_pass={}
for seq in seq_mut:
    mut_pass[seq]='pass'
    for i in seq_mut[seq]:
        if i not in mutations:
            mut_pass[seq]='fail'
            break

#%%
rz_fitness={}
for seq2 in mammals_rz:
    if mut_pass[seq2]=='pass':
        seq=mammals_rz[seq2]
        x=seq[0]+seq[5]+seq[8]+seq[18]+seq[24]+seq[28]+seq[29]+seq[35]+seq[45]+seq[46]+seq[47]+seq[48]+seq[57]
        if x in DATA_fit and x!='GCCGGCGTTGGTG':
            print (seq2, x,DATA_fit[x])
            rz_fitness[x]=DATA_fit[x]
        