#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 12:37:23 2018

@author: devin
"""

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)
#%%

plt.figure(1,figsize=(8,3))
plt.subplot(121)
plt.bar(['human-WT','human-SNP'],[0.69,1.79],edgecolor='black',linewidth=2,color=['steelblue','indianred'])

plt.ylabel('kobs (hrs)',weight='bold',fontsize=15)
plt.title('Salehi-Ashtiani, Luptak et al. ',weight='bold',fontsize=13)
plt.subplot(122)

plt.bar(['human-WT','human-SNP'],[0.72,0.23],edgecolor='black',linewidth=2,color=['steelblue','indianred'])
plt.ylabel('ribozyme activity',weight='bold',fontsize=15)
plt.title('Bendixsen et al.',weight='bold',fontsize=13)
plt.ylim()
plt.tight_layout()
plt.savefig('../Figures/CPEB3Phylo_20min.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
plt.figure(2,figsize=(6,3))

plt.scatter([0,20,40,80],[0,0.693213105,0.912211638,0.962398164],color='steelblue',edgecolor='black',s=200,linewidth=2)
plt.plot([0,20,40,80],[0,0.693213105,0.912211638,0.962398164],color='steelblue',zorder=0,linewidth=4)
plt.scatter([0,20,40,80],[0,0,0.002028634,0.118406422],color='indianred',edgecolor='black',s=200,linewidth=2)
plt.plot([0,20,40,80],[0,0,0.002028634,0.118406422],color='indianred',zorder=0,linewidth=4)
plt.ylabel('ribozyme activity',weight='bold',fontsize=15)
plt.xlabel('transcription time (min)',weight='bold',fontsize=15)
plt.savefig('../Figures/CPEB3Phylo_timecourse.png',bbox_inches='tight',dpi=1000,transparent=True)