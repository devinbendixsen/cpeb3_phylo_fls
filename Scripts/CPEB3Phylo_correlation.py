#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 09:26:28 2018

@author: devin
"""


import numpy
import pickle
sequences=pickle.load(open("../Pickle/CPEB3Phylo_sequences.p", "rb"))
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

sns.set(font_scale=2, font='bold')

#%% CLEAVED
cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_cleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_cleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_cleaved3.p","rb"))
xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved2.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved2.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 2':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 2',data=datas)
g = g.plot_joint(plt.scatter, color='b',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="b")
g = g.plot_joint(sns.regplot, color='b')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_cleaved1-2.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='b',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="b")
g = g.plot_joint(sns.regplot, color='b')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_cleaved1-3.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved2.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved2.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 2':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 2',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='b',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="b")
g = g.plot_joint(sns.regplot, color='b')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_cleaved2-3.png',bbox_inches='tight',dpi=1000)

#%% UNCLEAVED
cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_uncleaved3.p","rb"))
xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved2.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved2.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 2':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 2',data=datas)
g = g.plot_joint(plt.scatter, color='r',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="r")
g = g.plot_joint(sns.regplot, color='r')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_uncleaved1-2.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='r',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="r")
g = g.plot_joint(sns.regplot, color='r')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_uncleaved1-3.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved2.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved2.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 2':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 2',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='r',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="r")
g = g.plot_joint(sns.regplot, color='r')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_uncleaved2-3.png',bbox_inches='tight',dpi=1000)
#%% FRACTION CLEAVED
cleaved1=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved1.p","rb"))
cleaved2=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved2.p","rb"))
cleaved3=pickle.load(open("../Pickle/CPEB3Phylo_fraction_cleaved3.p","rb"))
xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved2.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved2.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 2':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 2',data=datas)
g = g.plot_joint(plt.scatter, color='gray',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="gray")
g = g.plot_joint(sns.regplot, color='gray')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_%cleaved1-2.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved1.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved1.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 1':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 1',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='gray',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="gray")
g = g.plot_joint(sns.regplot, color='gray')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_%cleaved1-3.png',bbox_inches='tight',dpi=1000)

xlist=[]
ylist=[]
xnum=[]
ynum=[]
for key in sequences:
    xnum.append(cleaved2.get(key,0))
    ynum.append(cleaved3.get(key,0))
    x=(cleaved2.get(key,0))
    y=(cleaved3.get(key,0))
    xlist.append(x)
    ylist.append(y)
datas=pd.DataFrame({'Replicate 2':xlist,'Replicate 3':ylist})
g=sns.JointGrid(x='Replicate 2',y='Replicate 3',data=datas)
g = g.plot_joint(plt.scatter, color='gray',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="gray")
g = g.plot_joint(sns.regplot, color='gray')
slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/CPEB3Phylo_%cleaved2-3.png',bbox_inches='tight',dpi=1000)