#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 12:25:26 2018

@author: devin
"""

#%% IMPORT NEEDED MODULES
import pickle
import matplotlib.pyplot as plt
from random import random, randint
import numpy as np
import os
import sys

#%% FITNESS LANDSCAPE SETTINGS
print('Loading fitness data...')
WT='GCCGGCGTTGGTG'
library = 'CPEB3Phylo_fitness'
data_file = "../Pickle/{}.p".format(library) #loads data from a dictionary pickle of genotype : relative fitness
DATA_fit = pickle.load( open(data_file,'rb'))

#%% GENERATE MUTATION OPTIONS
print('Calculating mutational options... (this could take a while)')
def mutcalc2(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
            if mut>1:
                break
    return mut
if os.path.isfile('../Pickle/{}_mut_options.p'.format(library))==True:
    mut_options = pickle.load(open('../Pickle/{}_mut_options.p'.format(library),'rb'))
else:
    mut_options={} # builds a dictionary of all of the possible mutational options for each genotype
    for seq in DATA_fit:
        mut_options[seq]=[]
    for seq in DATA_fit:
        for seq2 in DATA_fit:
            if mutcalc2(seq,seq2)==1:
                mut_options[seq].append(seq2)
    pickle.dump(mut_options, open('../Pickle/{}_mut_options.p'.format(library),'wb'))
#%%
robust={}
for seq in mut_options:
    x=[]
    for seq2 in mut_options[seq]:
        x.append(DATA_fit[seq2])
    robust[seq]=np.mean(x)
#%%
import pandas as pd
import seaborn as sns
plt.figure(figsize=[8,4])
y=pd.Series(list(robust.values()))
ax=sns.distplot(y, color='royalblue',hist=True,rug=True,kde=True)
plt.ylabel('kernal density estimate',weight='bold',fontsize=15,color='black')
plt.xlabel('robustness',weight='bold',fontsize=15,color='black')
plt.axvline(robust['GCCGGCGTTGGTG'],ls='dashed',color='black')
plt.savefig('../Figures/CPEB3Phylo_robust_dist.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
selection={}
for seq in mut_options:
    x=[]
    for seq2 in mut_options[seq]:
        x.append(DATA_fit[seq2]-DATA_fit[seq])
    selection[seq]=np.mean(x)
    #%%
import pandas as pd
import seaborn as sns
plt.figure(figsize=[8,4])
y=pd.Series(list(selection.values()))
ax=sns.distplot(y, color='royalblue',hist=True,rug=True,kde=True)
plt.ylabel('kernal density estimate',weight='bold',fontsize=15,color='black')
plt.xlabel('mean selection coefficient',weight='bold',fontsize=15,color='black')
plt.axvline(selection['GCCGGCGTTGGTG'],ls='dashed',color='black')
plt.savefig('../Figures/CPEB3Phylo_selection_dist.png',bbox_inches='tight',dpi=1000,transparent=True)