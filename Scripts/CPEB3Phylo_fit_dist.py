#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 10:35:54 2019

@author: devin
"""
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
sns.set_style('ticks')
DATA_fit=pickle.load(open('../Pickle/CPEB3Phylo_fitness.p','rb'))

#%%
plt.figure(figsize=[3,4])
plt.subplot(121)
cmap = matplotlib.cm.get_cmap('RdBu')

sns.violinplot(list(DATA_fit.values()),orient='v',cut=0,bw=0.15,color=cmap(np.median(list(DATA_fit.values()))),inner=None,scale='width')

plt.ylabel('ribozyme activity',weight='bold')

plt.ylim(0,1)
plt.yticks([0,0.25,0.5,0.75,1])
sns.despine(trim=True)
plt.xticks([])
plt.scatter(0,np.mean(list(DATA_fit.values())),color='black',marker='s')
plt.hlines(np.mean(list(DATA_fit.values())),xmin=-.3,xmax=0.3,linestyle='dashed')
plt.subplot(122)
extant=[0.05,0.92,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.92,0.92,0.05,0.05,0.92,0.05,0.03,0.92,0.05,0.05,0.05,0.05,0.05,0.94,0.05,0.94,0.94,0.94,0.94,0.94,0.05,0.92,0.94,0.92,0.92,0.05,0.92,0.92,0.05,0.05,0.05,0.72,0.23,0.92,0.92,0.92,0.92,0.41,0.92,0.92,0.92,0.92,0.92,0.92,0.92,0.92,0.92,0.03,0.09,0.92,0.92,0.92,0.92,0.92,0.92,0.92,0.92,0.42,0.92,0.92,0.92,0.93,0.92,0.05,0.92,0.92,0.92,0.05,0.92,0.92,0.92,0.92,0.05,0.93,0.93,0.92,0.93,0.92,0.92,0.05,0.03,0.93,0.92,0.93,0.92,0.92,0.92,0.92]
sns.violinplot(extant,orient='v',cut=0,bw=0.15,color=cmap(np.median(extant)),inner=None,scale='area')

plt.ylim(0,1)
plt.yticks([0,0.25,0.5,0.75,1],[])

sns.despine(trim=True)
plt.xticks([])
plt.scatter(0,np.mean(list(extant)),color='black',marker='s')
plt.hlines(np.mean(extant),xmin=-.3,xmax=0.3,linestyle='dashed')

plt.tight_layout(pad=0)

plt.savefig('../Figures/CPEB3Phylo_fitdist.png',bbox_inches='tight',dpi=1000,transparent=True)