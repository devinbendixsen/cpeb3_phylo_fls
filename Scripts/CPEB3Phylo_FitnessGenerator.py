#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 21:17:06 2018

@author: devinbendixsen
"""
# =============================================================================
# Master code to analyze CPEB3Phylo high throughput sequencing data.
# GOALS - To generate relative fitness measurements for each genotype.
# =============================================================================
#Import needed packages
import pickle
import numpy as np
#%%
#Define initial variables
library='CPEB3Phylo'
rz_WT_cleaved = 'GGGGGCCACAGCAGAAGCGTTCACGTCGCGGCCCCTGTCAGATTCTTGTGAATCTGCGAATTCTGCT'
rz_cleaved_seq = 'GGACCATTC'
weeks = 'GTTCGATCCGGTTCGC'
mutations=[1,6,9,19,25,29,30,36,46,47,48,49,58] #location of expected mutations
CPEB3Phylo_sequences=pickle.load(open('../Pickle/CPEB3Phylo_sequences.p','rb'))
#%%
##Identify where the data is stored.
seqDATA={}
seqDATA[1]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo1.fastq"
seqDATA[2]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo2.fastq"
seqDATA[3]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/CPEB3Phylo/Raw_DATA/CPEB3Phylo3.fastq"

#%%
#Load functions needed
def mutcalc(start,end,mutations): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    num_mut=0 #number of mutations in the correct spot
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
            if i in mutations:
                num_mut+=1
    return mut, num_mut

def slicer(sequence,mutations): #extracts the mutations located in mutations (list)
    sequence=sequence[mutations[0]-1]+sequence[mutations[1]-1]+sequence[mutations[2]-1]+sequence[mutations[3]-1]+sequence[mutations[4]-1]+sequence[mutations[5]-1]+sequence[mutations[6]-1]+sequence[mutations[7]-1]+sequence[mutations[8]-1]+sequence[mutations[9]-1]+sequence[mutations[10]-1]+sequence[mutations[11]-1]+sequence[mutations[12]-1]
    return sequence

#%%
num=0
DATA={}
for i in range(1,4):#builds a nested dictionary DATA with rz_cleaved, rz_uncleaved and relative_cleavage for each replicate
    DATA["rz_cleaved{}".format(i)] = {}
    DATA["rz_uncleaved{}".format(i)] = {}
    DATA["relative_cleavage{}".format(i)] = {}
    n=0
    for sequence in open (seqDATA[i]): #opens fastq sequencing files and extracts the 2nd line (sequence)
        n+=1
        if n==2:
            n=-2
            if weeks in sequence and len(sequence)>=(len(rz_WT_cleaved)+len(rz_cleaved_seq)+1) and 'CTATAGGA' not in sequence: #verifies that the weeks linker is in the sequence and determines that the length is long enough and the TATA box is not in it
                sequence=sequence.split(weeks,1)[0] #extracts the sequence upstream of the weeks linker
                sequence=sequence[-(len(rz_WT_cleaved)+len(rz_cleaved_seq)):] #extracts the length of the sequence and potential cleaved
                mut,num_mut=mutcalc(sequence[len(sequence)-len(rz_WT_cleaved):],rz_WT_cleaved,mutations)
                if mut<=len(mutations) and num_mut==mut: #uses mutcalc to determine that there are the correct number of mutations and they are in the correct location
                    if rz_cleaved_seq in sequence: #determines if it is an uncleaved sequence
                        sequence=sequence.split(rz_cleaved_seq,1)[1] #extracts sequence downstream of rz_cleaved_seq
                        sequence=slicer(sequence,mutations) #uses slicer to extract the mutational nucleotides
                        if sequence not in DATA["rz_uncleaved{}".format(i)] and sequence in CPEB3Phylo_sequences: #counts uncleaved sequences for each genotype
                            DATA["rz_uncleaved{}".format(i)][sequence]=1
                        elif sequence in CPEB3Phylo_sequences:
                            DATA["rz_uncleaved{}".format(i)][sequence]+=1
                    elif rz_cleaved_seq not in sequence: #determines if it is a cleaved sequence
                        sequence=sequence[-(len(rz_WT_cleaved)):] #extracts the full length sequence
                        sequence=slicer(sequence,mutations) #uses slicer to extract the mutational nucleotides
                        if sequence not in DATA["rz_cleaved{}".format(i)] and sequence in CPEB3Phylo_sequences: #counts cleaved sequences for each genotype
                            DATA["rz_cleaved{}".format(i)][sequence]=1
                        elif sequence in CPEB3Phylo_sequences:
                            DATA["rz_cleaved{}".format(i)][sequence]+=1
    for sequence in DATA["rz_uncleaved{}".format(i)]: #determines relative cleavage
        if sequence in DATA["rz_cleaved{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=(DATA["rz_cleaved{}".format(i)][sequence]/(DATA["rz_uncleaved{}".format(i)][sequence]+DATA["rz_cleaved{}".format(i)][sequence]))
        elif sequence not in DATA["rz_cleaved{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=0
    for sequence in DATA["rz_cleaved{}".format(i)]:
        if sequence not in DATA["relative_cleavage{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=1
    print("Number of cleaved {} : ".format(i),len(DATA["rz_cleaved{}".format(i)]))
    print("Number of uncleaved {} : ".format(i),len(DATA["rz_uncleaved{}".format(i)]))
    print("Number of total {} : ".format(i),len(DATA["relative_cleavage{}".format(i)]))

#%%
final_relative_cleavage={}
for sequence in DATA['relative_cleavage1']:
    if sequence in DATA['relative_cleavage2'] and sequence in DATA['relative_cleavage3']:
        a=[DATA['relative_cleavage1'][sequence],DATA['relative_cleavage2'][sequence],DATA['relative_cleavage3'][sequence]]
        final_relative_cleavage[sequence]=np.mean(a)
print("Number of sequences assessed : ", len(final_relative_cleavage))
#%%
pickle.dump(DATA, open("CPEB3Phylo_DATA.p", "wb"))
pickle.dump(DATA['rz_cleaved1'], open("../Pickle/{}_cleaved1.p".format(library), "wb"))
pickle.dump(DATA['rz_cleaved2'], open("../Pickle/{}_cleaved2.p".format(library), "wb"))
pickle.dump(DATA['rz_cleaved3'], open("../Pickle/{}_cleaved3.p".format(library), "wb"))
pickle.dump(DATA['rz_uncleaved1'], open("../Pickle/{}_uncleaved1.p".format(library), "wb"))
pickle.dump(DATA['rz_uncleaved2'], open("../Pickle/{}_uncleaved2.p".format(library), "wb"))
pickle.dump(DATA['rz_uncleaved3'], open("../Pickle/{}_uncleaved3.p".format(library), "wb"))
pickle.dump(DATA['relative_cleavage1'], open("../Pickle/{}_fraction_cleaved1.p".format(library), "wb"))
pickle.dump(DATA['relative_cleavage2'], open("../Pickle/{}_fraction_cleaved2.p".format(library), "wb"))
pickle.dump(DATA['relative_cleavage3'], open("../Pickle/{}_fraction_cleaved3.p".format(library), "wb"))
pickle.dump(final_relative_cleavage, open("../Pickle/{}_fitness.p".format(library), "wb"))
